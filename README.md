# Pass

A password generator.
https://pass.nexylan.dev

The tool comes from the [luzifer/password](https://github.com/Luzifer/password) project.

## Usage

```
$ curl --silent https://pass.nexylan.dev/v1/getPassword
XJVFSsibEd5WFV53QtDq
```

API options are described on the [official documentation](https://github.com/Luzifer/password#via-api).
